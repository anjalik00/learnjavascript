alert("The script is added!");

// Write a isValidPassword function 
// It accepts 2 arguments \: password and username
// Password must have: 
// - 8 characters 
// - cannot contain spaces 
// - cannot contain the username  
// If all requirements are met, then return true
// otherwise return false 

function isValidPassword(username, password) {
    if (username.length !== 0 && password.length >= 8 && password.indexOf(" ") === -1 && !password.indexOf(username)) {
        return true;
    }
    return false;
}

// write a function called average, should accept array of any length and then calculate the average. 
function calcAverage(arr) {
    let sum = 0;
    for (let number of arr) {
        sum += number;
    }
    let avg = sum / arr.length;
    return avg;
}

// pangram is a sentence that contains every letter of the alphabet. 
// Write a function called isPangram which checks to see if a given sentence contains every letter of the alphabet.
// Ignore casing 

function isPangram(sentence) {
    let alphabet = 'abcdefghijklmnopqrstuvwxyz';
    for (let letter of alphabet) {
        if (sentence.indexOf(letter.toLowerCase()) === -1) {
            return false;
        }
    }
    return true;
}

//write a function called getCard. It accepts no arguments.
// It returns a random playing card object.
// {value: 'K', suit: 'clubs'}
// Random value from - 2,3,4,5,6,7,8,9,10.J,Q,K,A
// Random suit from - clubs, spades, hearts and diamonds

function getCard() {
    const values = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
    const suit = ['clubs', 'spades', 'hearts', 'diamonds'];

    let randomIndex1 = Math.floor(Math.random() * values.length);
    let randomIndex2 = Math.floor(Math.random() * suit.length);

    const card = {
        value: values[randomIndex1],
        suit: suit[randomIndex2]
    }

    return card;
}


// Callback functions 
// callback functions in predefined array mthods.

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
numbers.forEach(function (num) {
    // console.log(num * num);
    num * num;
});

// or you couldve done the following way passing the variable in which function is stored.
// const printDouble = function(num){
//     console.log(num*num);
// }
// numbers.forEach(printDouble);

//foreach used on an array of objects 

const people = [
    {
        name: "Anjali",
        age: 20
    },
    {
        name: "Monali",
        age: 12
    },
    {
        name: "Atharva",
        age: 10
    }
]

people.forEach(function (element, index) {
    console.log(element.name.toUpperCase(), index);
});


//using map on an array  - map generates a new array after iterating through array contents. 
//map does not mutate the original array, hence its results need to captured in an array. 
// and values need to be returned so they can be added to the new array variable. 

const texts = ['baby', 'yacht', 'tea'];
const newTextCaps = texts.map(function (element) {
    return element.toUpperCase(); /// for every element in the array it turns it into uppercase then returns the element which is storesd in a variable. 
});

const newTextCaps2 = texts.map(function (element) { // more complicated array elements returned! 
    return {
        value: element.toUpperCase(),
        isPangram: isPangram(element)
    };
});

const justNames = people.map(function (element) {
    return element.name;
}); // creating a new array using only partial values of the previous objects array!


// Introduction to arrow functions - they are a syntactically compact alternative to regular funciton experssion. 
// they do not work in IE - but can be made to work using other tools. 

const square = function (x) {
    return x * x;
}

const square2 = (x) => {
    return x * x;
}

//find - returns the value of the first element in the array that satisfies the provided testing function(callback funciton)

const search = people.find((person) => {
    return person.name.indexOf("anjali");
})

//filter - Creates a new array with all elements that pass the test implemented by the provided function(callback function)

const numbers2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
const evens = numbers2.filter((num) => {
    return num % 2 === 0;
})

const age = people.filter((object) => {
    return object.age >= 11;
})

let query = 'anjali';
const results = people.filter((object) => {
    query.toLowerCase();
    const name = object.name.toLowerCase();
    return query.includes(name);
})

// this implict return type where the return keyword doesnt need to be used.
const odds = numbers2.filter(n => n % 2 === 1)


//every - tests whether all elements in the array pass the provided function(callback function). It returns a boolean.

const words = ['end', 'cat', 'dog'];

const isThreecheck = words.every((string) => {
    return string.length === 3;
});

const isNameCheck = people.every((object) => {
    return object.name.toLowerCase() === 'anjali';
})


//some - cares if ANY of the array elements pass the test function(callback function)

const hasZ = people.some((object) => {
    return object.name.toLowerCase().includes('z');
})

//sort - there are 2 rules to determine if sorted in ascending or descending order. Each paramter represents one element of the array!
// just sort alone is not useful for numbers. 



//reduce - built in array method which has callback passed to it.
// Executes a reducer function on each element of the array, resulting in a single value. thus goes through the array to get a single value

function makeDeck() {
    const deck = [];
    const values = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
    const suits = ['clubs', 'spades', 'hearts', 'diamonds'];

    for (let value of values) {
        for (let suit of suits) {
            deck.push({ value, suit });
        }
    }
    return deck;
}

// function drawCard(deck){
//     return deck.pop();
// }

// the following way isnt great because you keep having to pass around the deck  - alternatively this can be done on the deck object itself using this.

// const myDeck = makeDeck();
// const card1 = drawCard(myDeck);

// we can use objects to store the deck created and then add functions on top of the object using this keyword 



// the following exercise is a good example of grouping values with functionality in a logical way. 
const myDeck = {
    deck: [],
    drawnCards: [],
    values: ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'],
    suits: ['clubs', 'spades', 'hearts', 'diamonds'],
    initaliseDeck() {
        const { deck, values, suits } = this;
        for (let value of values) {
            for (let suit of suits) {
                deck.push({ value, suit });
            }
        }
        return deck;
    },
    drawCard() {
        const card = this.deck.pop();
        this.discardCard(card);
        return card;
    },
    drawMultipleCards(numCards) {
        const cards = [];
        for (let i = 0; i <= numCards; i++) {
            const card = this.drawCard();
            cards.push(card);
        }
        return cards;
    },
    discardCard(card) {
        this.drawnCards.push(card);
    },
    shuffle() { // this is the fisher yates algorithm to shuffle an array
        for (let i = this.deck.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [this.deck[i], this.deck[j]] = [this.deck[j], this.deck[i]];
        }
        return this.deck;
    }
}

// how to create multiple decks since the above deck represented one deck completely.
// throw it inside a function and just call the function for a new deck, it will return a new object each time.
// const makeDeck = () => {
//     return {
//         deck: [],
//         drawnCards: [],
//         values: ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'],
//         suits: ['clubs', 'spades', 'hearts', 'diamonds'],
//         initaliseDeck() {
//             const { deck, values, suits } = this;
//             for (let value of values) {
//                 for (let suit of suits) {
//                     deck.push({ value, suit });
//                 }
//             }
//             return deck;
//         },
//         drawCard() {
//             const card = this.deck.pop();
//             this.discardCard(card);
//             return card;
//         },
//         drawMultipleCards(numCards) {
//             const cards = [];
//             for (let i = 0; i <= numCards; i++) {
//                 const card = this.drawCard();
//                 cards.push(card);
//             }
//             return cards;
//         },
//         discardCard(card) {
//             this.drawnCards.push(card);
//         },
//         shuffle() { // this is the fisher yates algorithm to shuffle an array
//             for (let i = this.deck.length - 1; i > 0; i--) {
//                 let j = Math.floor(Math.random() * (i + 1));
//                 [this.deck[i], this.deck[j]] = [this.deck[j], this.deck[i]];
//             }
//             return this.deck;
//         }
//     }
// }
