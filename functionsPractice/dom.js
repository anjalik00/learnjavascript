//each object in the array represents a game 

const warriorsGames = [{
    awayTeam: {
      team: 'Golden State',
      points: 119,
      isWinner: true
    },
    homeTeam: {
      team: 'Houston',
      points: 106,
      isWinner: false
    }
  },
  {
    awayTeam: {
      team: 'Golden State',
      points: 105,
      isWinner: false
    },
    homeTeam: {
      team: 'Houston',
      points: 127,
      isWinner: true
    }
  },
  {
    homeTeam: {
      team: 'Golden State',
      points: 126,
      isWinner: true
    },
    awayTeam: {
      team: 'Houston',
      points: 85,
      isWinner: false
    }
  },
  {
    homeTeam: {
      team: 'Golden State',
      points: 92,
      isWinner: false
    },
    awayTeam: {
      team: 'Houston',
      points: 95,
      isWinner: true
    }
  },
  {
    awayTeam: {
      team: 'Golden State',
      points: 94,
      isWinner: false
    },
    homeTeam: {
      team: 'Houston',
      points: 98,
      isWinner: true
    }
  },
  {
    homeTeam: {
      team: 'Golden State',
      points: 115,
      isWinner: true
    },
    awayTeam: {
      team: 'Houston',
      points: 86,
      isWinner: false
    }
  },
  {
    awayTeam: {
      team: 'Golden State',
      points: 101,
      isWinner: true
    },
    homeTeam: {
      team: 'Houston',
      points: 92,
      isWinner: false
    }
  }
]

const makeChart = (games, targetTeam) => {
    const ulParent = document.createElement('ul');
    for (let game of games) {
        const { homeTeam, awayTeam } = game;

        const gameLi = document.createElement('li');
        gameLi.innerHTML = getScoreLine(game);
        gameLi.classList.add(isGameWinner(game,targetTeam) ? 'win' : 'loss');

        ulParent.appendChild(gameLi);
    }
    return ulParent;
};


const isGameWinner = (game, targetTeam) => {
    const { homeTeam, awayTeam } = game;
    const target = homeTeam === targetTeam ? homeTeam : awayTeam;
    return target.isWinner;
}



const getScoreLine = ({ homeTeam, awayTeam}) => {
    const { team: hteam, points: hpoints } = homeTeam;
    const { team: ateam, points: apoints } = awayTeam;
    const teamNames = `${ateam.team} @ ${hteam.team}`;

    let scoreLine;
        if (apoints > hpoints) {
            scoreLine = `<b>${apoints}</b>-${hpoints}`;
        } else {
            scoreLine = `${apoints}-<b>${hpoints}</b>`;
    }
    
    return `${teamNames} ${scoreLine}`;
}


const chart = makeChart(warriorsGames, 'Golden State');
document.body.prepend(chart);

const chart1 = makeChart(warriorsGames, 'Houston');
document.body.prepend(chart1);




