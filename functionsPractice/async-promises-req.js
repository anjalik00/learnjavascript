// // creating promises
// const willGetDog = new Promise((resolve, reject) => {
//     const random = Math.random();
//     if (random < 0.5) {
//         resolve();
//     } else {
//         reject()
//     }
// })

// // how to interact with a promise

// willGetDog.then(()=>{ // code will run if promise is resolved.
//     console.log('resolved'); 
// })

// willGetDog.catch(()=>{ // code will run if promise is rejected.
//     console.log('rejected'); 
// })

// returning promises from Functions 

// adding a setTimeout to modelling some wort of wait that could occurr.
// const willGetDog = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         const random = Math.random();
//         if (random < 0.5) {
//             resolve();
//         } else {
//             reject()
//         }
//     }, 5000);
// })


// willGetDog.then(()=>{ // code will run if promise is resolved.
//     console.log('resolved'); 
// })

// willGetDog.catch(()=>{ // code will run if promise is rejected.
//     console.log('rejected'); 
// })

// returning promises from Functions 

const makeDogPromise = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const random = Math.random();
            if (random < 0.5) {
                resolve();
            } else {
                reject()
            }
        }, 5000);
    })
};


makeDogPromise() // following methods are chained to one promise - if called separately they could refer to 2 different objects of the promise. 
    .then(() => { // code will run if promise is resolved.
        console.log('resolved');
    })
    .catch(() => { // code will run if promise is rejected.
        console.log('rejected');
    });

//OR you could save the promise object returned to a variabel 
// const promise = makeDogPromise()
// promise.then(() => { });
// promise.catch(() => { });


// rejecting/resolving a promise with a value!

const fakeRequest = (url) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const ran = Math.random();
            if (rand < 0.3) {
                reject({status:404});
            } else {
                const pages = {
                    '/users': [
                        {
                            id: 1, username: 'anjali'
                        }
                    ]
                }
                const data = pages[url]
                resolve({status: 200, data});
            }
        }, 3000);
    })
}

fakeRequest('/users').then(() => {
    console.log('worked');
}).catch((res) => {
    console.log(res.status);
});