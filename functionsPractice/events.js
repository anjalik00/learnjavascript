const btn = document.querySelector('button');
btn.addEventListener('mouseover', () => { 
    console.log("hi");
    const height = Math.floor(Math.random() * window.innerHeight);
    const width = Math.floor(Math.random() * window.innerWidth);
    btn.style.top = `${height}px`;
    btn.style.left = `${width}px`;
});

btn.addEventListener('click', () => { 
    btn.innerText = 'You got me';
    document.body.style.backgroundColor = 'green';
});



// add click listener to multiple elements!!
const colors = ['red', 'green', 'orange', 'blue', 'purple', 'yellow'];

const container = document.querySelector('#boxes');

for (let color of colors) {
    const box = document.createElement('div');
    box.style.backgroundColor = color;
    box.classList.add('box');
    container.append(box);
    box.addEventListener('click', () => {
        console.log("clicked");
        console.log(box.style.backgroundColor);
    });
}