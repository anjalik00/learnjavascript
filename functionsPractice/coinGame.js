function isTouching(a, b) {
	const aRect = a.getBoundingClientRect();
	const bRect = b.getBoundingClientRect();

	return !(
		aRect.top + aRect.height < bRect.top ||
		aRect.top > bRect.top + bRect.height ||
		aRect.left + aRect.width < bRect.left ||
		aRect.left > bRect.left + bRect.width
	);
}

const player = document.querySelector('#player');
const coin = document.querySelector('#coin');

window.addEventListener('keyup', (e) => {
    if (e.key === 'ArrowDown' || e.key === 'Down') {
        const currentTop = extractPosition(player.style.top);
        player.style.top = `${currentTop + 50}px`;
    } else if (e.key === 'ArrowUp' || e.key === 'Up') {
        const currentTop = extractPosition(player.style.top);
        player.style.top = `${currentTop - 50}px`;
    } else if (e.key === 'ArrowRight' || e.key === 'Right') {
        const currentLeft = extractPosition(player.style.left);
        player.style.left = `${currentLeft + 50}px`;
        player.style.transform = 'scale(1,1)';
    } else if (e.key === 'ArrowLeft' || e.key === 'Left') {
        const currentLeft = extractPosition(player.style.left);
        player.style.left = `${currentLeft - 50}px`;
        player.style.transform = 'scale(-1,1)';
    }

    if (isTouching(player, coin)) {
        moveCoin();
    }

});

const extractPosition = (pos) => {
    if (!pos) return 100;
    let number = parseInt(pos.slice(0, -2));
    return number;
}

const moveCoin = () => {
    const height = Math.floor(Math.random() * window.innerHeight);
    const width = Math.floor(Math.random() * window.innerWidth);
    coin.style.top = `${height}px`;
    coin.style.width = `${width}px`;
}

moveCoin();