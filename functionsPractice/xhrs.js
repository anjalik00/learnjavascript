//XHRs
// 1. creating a request using XMLHttpRequest
// 2. open url
// 3. attach callbacks (add load and error callbacks to event listener)
// 4. send request

const firstReq = new XMLHttpRequest();

//2 branching paths below

firstReq.addEventListener('load', () => { 
    console.log("worked");
    // this.responseText;  // this will not work because we are using an arrow function
    const data = JSON.parse(firstReq.responseText);
   
    const filmURL = data.results[0].films[0];
    const filmReq = new XMLHttpRequest();
    filmReq.open("GET", `${filmURL}`);
    filmReq.addEventListener('load',()=> {
        const filmData = JSON.parse(filmReq.responseText);
        console.log(filmData);
    });
    filmReq.addEventListener('error',(e)=> {
        console.log(e);
    })
    filmReq.send();
});

firstReq.addEventListener('error', () => {
    console.log("didnt work");
});

firstReq.open("GET", "http://swapi.dev/api/planets/");
firstReq.send(); // gets handled by the browser
console.log('Request sent!');


//Requests via FETCH 
//1. use method fetch and pass in url, method should return a promise.
//2. promise is either resolved or rejected
//3. if resolved it is resolved with a response object.

fetch("http://swapi.dev/api/planets/").then((response) => {
    if (response.ok) {
        response.json().then((response2) => {
            console.log(response2);
        })
    }
}).catch((error) => {
    console.log(error); 
});

// chaining requests 

fetch("http://swapi.dev/api/planets/").then((response) => {
    if (response.ok) {
        return response.json(); // json returns a promise 
    }
}).then((response2) => {
    const url = response2.results[0].films[0];
    return fetch(url); // fetch returns a promise hence returned
}).then((response3) => {
    if (response3.ok) {
        return response3.json(); //json returns a promise thus returned
    }
}).then((response4) => {
    console.log(response4.title);
})
.catch((error) => {
    console.log(error); 
});


// Requests via Axios 
//1. use the get method and pass through the url
axios
    .get("http://swapi.dev/api/planets/") // returns a promise
    .then((response) => {
        console.log(response.data);
        for (let planet of response.data.results) {
            console.log(planet.name);
        }
        return axios.get(response.data.next)
    })
    .then((response) => {
        console.log(response);
    })
    .catch((error) => {
        console.log(error);
    });



// async and await
async function add(x, y) {
    if (typeof x !== 'number' || typeof y !== 'number') {
        throw 'X and Y need to be numbers';
    }
    return x + y;
}

// function getPlanets() {
//     return axios.get("http://swapi.dev/api/planets/");
// }
// getPlanets().then((res) => {
//     console.log(res.data);
// })

//doing the above using async funcitons

async function getPlanets() {
    const res = await axios.get("http://swapi.dev/api/planets/"); // saving the value with which the promise has resolved with to res.
    console.log(res.data);
}

getPlanets().catch((err) => {
    console.log(err);
});

// doing above function with try and catch block for error handling. 
async function getPlanets2() {
    try {
        const res = await axios.get("http://swapi.dev/api/planets/");
        console.log(res.data);
    } catch (e) {
        console.log(e);
    }
}
